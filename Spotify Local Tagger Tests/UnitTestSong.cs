﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpotifyAPI.Web.Models;
using Spotify_Local_Tagger;

namespace Spotify_Local_Tagger_Tests
{
    /// <summary>
    /// Description résumée pour UnitTestSong
    /// </summary>
    [TestClass]
    public class UnitTestSong
    {

        public UnitTestSong()
        {

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active, ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Tests the initialization of the local matching string of 
        /// a local song + its processing.
        /// </summary>
        [TestMethod]
        public void testLocalInitMatchingStringV1()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            string theMatchingString = song.getMatchingString();

            Assert.AreEqual(theMatchingString, "ADRIENPITZMYTESTMUSICHEHE");

        }

        /// <summary>
        /// Tests the initialization of the local matching string
        /// of a local song only.
        /// </summary>
        [TestMethod]
        public void testLocalInitMatchingStringV2()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("Here is a test");

            Assert.AreEqual(song.getMatchingString(), "Here is a test");
        }

        /// <summary>
        /// Tests if all the accentuated "A" are replaced by an "A".
        /// </summary>
        [TestMethod]
        public void testLocalRemoveAccentsA()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("àáâãä");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "AAAAA");
        }

        /// <summary>
        /// Tests if all the accentuated "E" are replaced by an "E"
        /// </summary>
        [TestMethod]
        public void testLocalRemoveAccentsE()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("èéêë");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "EEEE");
        }

        /// <summary>
        /// Tests if all the accentuated "I" are replaced by an "I"
        /// </summary>
        [TestMethod]
        public void testLocalRemoveAccentsI()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("ìíîñï");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "IIINI");
        }

        /// <summary>
        /// Tests if all the accentuated "O" are replaced by an "O"
        /// </summary>
        [TestMethod]
        public void testLocalRemoveAccentsO()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("òóôõö");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "OOOOO");
        }

        /// <summary>
        /// Tests if all the accentuated "U" are replaced by an "U"
        /// </summary>
        [TestMethod]
        public void testLocalRemoveAccentsU()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("ùúûü");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "UUUU");
        }

        /// <summary>
        /// Tests if all the accentuated "Y" are replaced by an "Y"
        /// </summary>
        [TestMethod]
        public void testLocalRemoveAccentsY()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("ýÿ");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "YY");
        }

        /// <summary>
        /// Test the get track method
        /// </summary>
        [TestMethod]
        public void testLocalGetTrack()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            Assert.AreEqual(song.getTrack(), theFile);
        }

        /// <summary>
        /// Test the Spotify suggestion.
        /// </summary>
        [TestMethod]
        public void testLocalSpotifySuggestion()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            SpotifySong spotSong = new SpotifySong(null, null);

            song.setSpotifySuggestion(spotSong);

            Assert.AreEqual(song.hasSuggestion(), true);
        }

        /// <summary>
        /// Tests the deletion of any kind of punctuation.
        /// </summary>
        [TestMethod]
        public void testDeletePunctuation()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("/.?a!'’,\"*~:;");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "A");
        }

        /// <summary>
        /// Test the replacement of "&" by "AND"
        /// </summary>
        [TestMethod]
        public void testSpecialAndReplacement()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("Adrien & Jérémy & Laura and Bérangère");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "ADRIENANDJEREMYANDLAURAANDBERANGERE");
        }

        /// <summary>
        /// Tests remove digit before song
        /// </summary>
        [TestMethod]
        public void testRemoveDigitBeforeSong()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("0000001254. My Song - Is A beautiful song");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "MYSONGISABEAUTIFULSONG");
        }

        [TestMethod]
        public void testRemoveDigitBeforeSongV2()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("0000001254- My Song - Is A beautiful song");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "MYSONGISABEAUTIFULSONG");
        }

        [TestMethod]
        public void testRemoveDigitBeforeSongV3()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("000000a1254- My Song - Is A beautiful song");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "000000A1254MYSONGISABEAUTIFULSONG");
        }

        [TestMethod]
        public void testFeaturingRemovals()
        {
            // 1. Initialize the local song
            TagLib.File theFile = TagLib.File.Create("Adrien Pitz - My Test Music héhé (+ lyrics) [ORIGINAL VéRSION].mp3");

            // 2. Create Instance
            LocalSong song = new LocalSong(theFile);

            // 3. Test
            song.initMatchingString("Adrien ft. Laura featuring Jérémy feat Jérôme - Lalala");
            song.processMatchingString();

            Assert.AreEqual(song.getMatchingString(), "ADRIENLAURAJEREMYJEROMELALALA");
        }

    }
}
